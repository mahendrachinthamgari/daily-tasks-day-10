/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

//1. Fetch all the users.

fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    });


// 2. Fetch all the todos

fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    });


//3. Use the promise chain and fetch the users first and then the todos.

fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    })
    .then(() => {
        return fetch('https://jsonplaceholder.typicode.com/todos')
    })
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    });


//4. Use the promise chain and fetch the users first and then all the details for each user.

fetch('https://jsonplaceholder.typicode.com/users')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        return detailsOfEachUser(data);
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    });



function detailsOfEachUser(data) {
    return new Promise((resolve, reject) => {
        const userDetailsPromises = data.map((user) => {
            return fetch(`https://jsonplaceholder.typicode.com/users?id=${user.id}`);
        });
        const userDetails = userDetailsPromises.map((user) => {
            return user.then((data) => data.json());
        });
        Promise.all(userDetails)
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                reject(err);
            });
    });
}

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo.


fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        return detailsOfEachUserAssociatedTodo(data[0]);
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    });



function detailsOfEachUserAssociatedTodo(data) {
    return new Promise((resolve, reject) => {
        fetch(`https://jsonplaceholder.typicode.com/users?id=${data.id}`)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                resolve(data);
            })
            .catch((err) => {
                reject(err);
            });
    });
}